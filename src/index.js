import "./assets/styles/styles.scss";
import "./index.scss";

const articleContainerElement = document.querySelector(".articles-container");

const createArticles = (articles) => {
  const articlesDOM = articles.map((article) => {
    const articleDOM = document.createElement("div");
    articleDOM.classList.add("article");
    articleDOM.innerHTML = `
    <img
    src="${article.img}"
    alt="userProfile"
  />
  <h2>${article.title}</h2>
  <p class="article-author">${article.author} - ${article.category}</p>
  <p class="article-content">
  ${article.content}
  </p>
  <div class="article-action">
  <button class="btn btn-danger" data-id=${article._id}>Delete</button>
</div>
    `;
    return articleDOM;
  });
  console.log(articlesDOM);
  articleContainerElement.innerHTML = "";
  articleContainerElement.append(...articlesDOM);
  const deleteBtns = articleContainerElement.querySelectorAll(".btn-danger");
  deleteBtns.forEach((button) => {
    button.addEventListener("click", async (event) => {
      try {
        const target = event.target;
        const articleId = target.dataset.id;
        const response = await fetch(
          `https://restapi.fr/api/articlesblogmc/${articleId}`,
          {
            method: "DELETE",
          }
        );
        const body = await response.json();
        console.log(body);
        fetchArticle();
      } catch (error) {
        console.log("error :", error);
      }
    });
  });
};

const fetchArticle = async () => {
  try {
    const response = await fetch("https://restapi.fr/api/articlesblogmc");
    const articles = await response.json();
    console.log(articles);
    createArticles(articles);
  } catch (error) {
    console.log("error :", error);
  }
};

fetchArticle();
